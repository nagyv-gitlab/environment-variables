# Environment variables

The [GitLab docs collects all the predefined environment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) available during a CI/CD run. While that page is great, it requires an understanding of branch and merge requests pipelines, that I don't have. This project build out branch and merge request pipelines to learn about the environment provided by GitLab.

## Recommended usage

You have an idea what variable you might need, but you don't know what setup will it provide and what the expected value might be. 

In this situation, you check the below list of scenarios, and open the job where you expect your variable to be present, and do a search for it on the page.

## Scenarios

1. Commit to main, directly
   - [without any workflow rules](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567747301)
   - [with the Branch pipeline workflow](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567792990)
   - [with the Merge request workflow](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567774576)
1. Commit to a branch, without opening an MR
   - [without any workflow rules](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567757869)
   - [with the Branch pipeline workflow](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567794869)
   - with the Merge request workflow - no pipeline created
1. Commit to a branch, with opening an MR at the same time
   - [without any workflow rules](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567760480)
   - [with the Branch pipeline workflow](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567796247)
   - [with the Merge request workflow](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567782263)
1. Commit to a branch, with an open MR
   - [without any workflow rules](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567762451)
   - [with the Branch pipeline workflow](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567799304)
   - [with the Merge request workflow](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567784510)
1. Merge an MR to main
   - [without any workflow rules](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567765655)
   - [with the Branch pipeline workflow](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567802457)
   - [with the Merge request workflow](https://gitlab.com/nagyv-gitlab/environment-variables/-/jobs/1567787329)
